package scalaframe


import org.scalacheck.{Arbitrary, Gen}

class BitSeriesTest extends UnitTest {

  val dataInt = for {
    ids <- Gen.containerOf[Array, Int](Arbitrary.arbitrary[Int])
    xs <- Gen.containerOfN[Array, Boolean](ids.length, Arbitrary.arbitrary[Boolean])
  } yield (xs, ids)

  "BitSeries(xs, ids)" should "should create Series object for any Same length arrays." in {
    forAll(dataInt) {(dataInt: (Array[Boolean], Array[Int])) =>
      val (xs, ids) = dataInt
      val s = BitSeries(xs, ids)
      s.size shouldEqual ids.length
    }
  }

  it should "throw IllegalArgumentException when data and index are not of same size" in {
    intercept[IllegalArgumentException] {
      BitSeries(Array(true, false, true, false), Array(1,2, 3))
    }
  }

  it should "throw IndexOutOfBoundsException when trying to extract value for a non-existant index" in {
    intercept[IndexOutOfBoundsException] {
      val s = BitSeries(Array(true,true,false), Array("a","b","c"))
      s("0")
    }
  }

  it should "throw IndexOutOfBoundsException when trying to update value of a non-existant index" in {
    intercept[IndexOutOfBoundsException] {
      val s = BitSeries(Array(true,true,false), Array("a","b","c"))
      s.update("0", true)
    }
  }
}
