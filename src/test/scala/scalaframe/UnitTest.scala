package scalaframe

import org.scalatest.prop.PropertyChecks
import org.scalatest.{FlatSpec, Matchers}

abstract class UnitTest extends FlatSpec with Matchers with PropertyChecks