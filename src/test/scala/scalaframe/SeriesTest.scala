package scalaframe

import org.scalacheck.{Arbitrary, Gen}

class SeriesTest extends UnitTest {

  val dataIntDouble = for {
    ids <- Gen.containerOf[Array, Int](Arbitrary.arbitrary[Int])
    xs <- Gen.containerOfN[Array, Double](ids.length, Arbitrary.arbitrary[Double])
  } yield (xs, ids)

  "Series(xs, ids)" should "should create Series object for any Same length arrays." in {
    forAll(dataIntDouble) {(dataIntDouble: (Array[Double], Array[Int])) =>
      val (xs, ids) = dataIntDouble
      val s = Series(xs, ids)
      s.size shouldEqual xs.length
    }
  }

  it should "throw IllegalArgumentException when data and index are not of the same size" in {
    intercept[IllegalArgumentException] {
      Series(Array(1,2,3), Array(1,2))
    }
  }

  it should "throw IndexOutOfBoundsException when trying to extract value for a non-existant index" in {
    intercept[IndexOutOfBoundsException] {
      val s = Series(Array(1,2,3), Array("a","b","c"))
      s("0")
    }
  }

  it should "throw IndexOutOfBoundsException when trying update value of a non-existant index" in {
    intercept[IndexOutOfBoundsException] {
      val s = Series(Array(1,2,3), Array("a","b","c"))
      s.update("0", 0)
    }
  }

}
