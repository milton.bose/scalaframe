package scalaframe

import breeze.linalg.{BitVector, TensorLike}

/**
  * BitSeries class definition
  *
  * @param data
  * @param index
  * @tparam I
  */
class BitSeries[I](val data: BitVector, val index: Array[I])
  extends DataSeries[I, Boolean, BitVector]
    with TensorLike[I, Boolean, BitSeries[I]] {

  /**
    *
    * @return
    */
  def repr: BitSeries[I] = this

  /**
    *
    * @return
    */
  override def toString = iterator.map{ case (k, v) => k + " -> " + v }.mkString("BitSeries(", ", ", ")")

}

/**
  * BitSeries Companion Object
  */
object BitSeries {

  /**
    *
    * @param data
    * @param index
    * @tparam I
    * @return
    */
  def apply[I](data: Array[Boolean], index: Array[I]) = new BitSeries(BitVector(data:_*), index)

  def apply(data: Array[Boolean]) = {
    val index = Array.range(0, data.length)
    new BitSeries(BitVector(data:_*), index)
  }

}

