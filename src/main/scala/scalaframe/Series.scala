package scalaframe

import breeze.linalg.{DenseVector, TensorLike}

/**
  * Series Class definition
  *
  * @param data
  * @param index
  * @tparam I
  * @tparam V
  */
class Series[I, @specialized(Int, Long, Float, Double) V](val data: DenseVector[V],
                                                          val index: Array[I])
  extends DataSeries[I, V, DenseVector[V]]
    with TensorLike[I, V, Series[I, V]] {

  /**
    *
    * @return
    */
  def repr: Series[I, V] = this

  /**
    *
    * @return
    */
  override def toString = iterator.map{ case (k, v) => k + " -> " + v }.mkString("Series(", ", ", ")")

}

/**
  * Series Companion object
  */
object Series {

  /**
    * Instantiates a Series singleton
    *
    * @param data
    * @param index
    * @tparam I
    * @tparam V
    * @return
    */
  def apply[I, @specialized(Int, Long, Float, Double) V](data: Array[V],
                                                         index: Array[I]) = new Series(DenseVector(data), index)

  def apply[@specialized(Int, Long, Float, Double) V](data: Array[V]) = {
    val index = Array.range(0, data.length)
    new Series(DenseVector(data), index)
  }

}
