package scalaframe

import breeze.linalg.{Vector, Tensor}

/**
  * Created by mbose on 10/22/16.
  */
trait DataSeries[I, V, +VS <: Vector[V]] extends Tensor[I, V] {

  val data: VS
  val index: Array[I]

  require(index.length == data.length, "index and data must have the same length")

  /**
    *
    * @return
    */
  lazy val keySet: Set[I] = index.toSet

  /**
    *
    * @return
    */
  def iterator: Iterator[(I, V)] = data.iterator.map { case (i, v) => (index(i), v) }

  /**
    *
    * @return
    */
  def activeIterator: Iterator[(I, V)] = iterator


  /**
    *
    * @return
    */
  def keysIterator: Iterator[I] = index.toIterator

  /**
    *
    * @return
    */
  def activeKeysIterator = keysIterator

  /**
    *
    * @return
    */
  def valuesIterator: Iterator[V] = data.valuesIterator

  /**
    *
    * @return
    */
  def activeValuesIterator = valuesIterator

  /**
    *
    * @return
    */
  def size: Int = index.length

  /**
    *
    * @return
    */
  def activeSize = size

  /**
    *
    * @param i
    * @return
    */
  def apply(i: I): V = {
    if(!keySet.contains(i)) throw new IndexOutOfBoundsException(i + " is not a valid index")
    data(index.indexOf(i))
  }

  /**
    *
    * @param i
    * @param v
    */
  def update(i: I, v: V) = {
    if(!keySet.contains(i)) throw new IndexOutOfBoundsException(i + " is not a valid index")
    data.update(index.indexOf(i), v)
  }

  /**
    *
    * @return
    */
  override def toString = iterator.map{ case (k, v) => k + " -> " + v }.mkString("DataSeries(", ", ", ")")

}
