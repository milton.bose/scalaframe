# Scalaframe
### DataFrame and Series for Scala

[![build status](https://gitlab.com/milton.bose/scalaframe/badges/develop/build.svg)](https://gitlab.com/milton.bose/scalaframe/commits/develop)
[![coverage report](https://gitlab.com/milton.bose/scalaframe/badges/develop/coverage.svg)](https://gitlab.com/milton.bose/scalaframe/commits/develop)
